
<?php
if (isset($_GET['ok'])) {
    ?>

    <script> alert('Obrigado por se inscrever!!!')</script>
    <?php
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Cestas dos dia dos namorados</title>
        <link rel="stylesheet" href="front.css">


    </head>
    <body>


        <section class="container main">
            <div class="participe">

                <img src="imagem/participe.png">

            </div>
            <div class="container">
                  <div class="imgcoracao">
                    <img src="imagem/coracao.png">
                </div>
                
                <div class="containerformulario">
                    <div class="containerinformativo">
                        <div class="informativo">
                            <h3>Sorteio será realizado na aula do Prof. Davi dia:_/_/___. </h3>
                        </div>
                    </div>
                    <form method="post" class="formulario" action="php/post.php">
                        <h4> Preencha o formulário abaixo e concorra a uma cesta dos dias dos namorados.</h4>
                        <h3>Nome</h3>
                        <input type="text" name="nome" placeholder="Informe seu nome"/>
                        <h3>Email</h3>
                        <input type="email" name="email" placeholder="Insira seu E-mail">
                        <h3>Telefone</h3>
                          <input type="text" name="whatsapp" placeholder="informe seu whatsapp">
                        <h3>CPF</h3>
                        <input type="text" name="cpf" data-mask="000.000.000-00" maxlength="11" placeholder="informe seu CPF (Apenas números)"/>
                       
                        <input type="submit" value="Enviar">
                    </form>
                   
                </div>
              
            </div>  
        </section>


    </body>
</html>