<?php

class Database {
   
    protected $db;
    protected $db_sem_bd;
    protected $erro = "sucesso";

    public function __construct() {

        $db_host = "localhost";
        $db_nome = "namorados";
        $db_usuario = "root";
        $db_senha = '';
        $db_porta = '3306';


        $db_driver = "mysql";
        try {
            $this->db = new PDO("$db_driver:host=$db_host; port=$db_porta; dbname=$db_nome", $db_usuario, $db_senha);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            $this->erro = "Erro não tratado: " . $e->getMessage();
        }
    }

    public function __destruct() {
        return $this->db = null;
    }

    public function conexao() {
        return $this->db;
    }

    public function v_erro() {
        return $this->erro;
    }

    public function Beguin() {
        $this->conexao();
        if ($this->conexao()->beginTransaction()) {
            return $this->db;
        }
        return $this->erro;
    }

    public function Commit() {
        $this->conexao()->Commit();
    }

    public function Rollback() {
        $this->conexao()->rollback();
    }

}


?>
